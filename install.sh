echo -e "\e[1;33m - Add repo to python3.6 for Ubuntu 20.04 \e[0m"
sudo add-apt-repository ppa:deadsnakes/ppa -y

echo -e "\e[1;33m - Make system up to date \e[0m"
sudo apt-get update -y
sudo apt-get upgrade -y
sudo apt-get full-upgrade -y

echo -e "\e[1;33m - Install Python framework \e[0m"
sudo apt-get install python3.7 python3.7-dev -y

echo -e "\e[1;33m - Install pipx and related required packages and reload profile \e[0m"
sudo apt-get install python3.7-venv python3-pip -y
# Note : SLG, 16/11/2022.
# Ligne suivante nécessaire pour installer le PC de quentin.
# Je ne comprend pas pourquoi le apt-get install de python3-pip n'est
# pas suffisant...
# curl https://bootstrap.pypa.io/get-pip.py | sudo -H python3.6
# +
# apt-get install libpq-dev -y
python3.7 -m pip install --user pipx
python3.7 -m pipx ensurepath
source ~/.profile

echo -e "\e[1;33m - Install database engine and add extension by default \e[0m"
sudo apt-get install postgresql -y
sudo su postgres -c "psql -d template1 -c 'create extension unaccent;'"

echo -e "\e[1;33m - Install libraries required by Odoo \e[0m"
sudo apt-get install libxml2-dev libxslt1-dev libldap2-dev libsasl2-dev libssl-dev -y
sudo apt-get install libtiff5-dev libjpeg8-dev zlib1g-dev libfreetype6-dev liblcms2-dev libwebp-dev tcl8.6-dev tk8.6-dev python-tk -y
sudo apt-get install node-less -y

echo -e "\e[1;33m - Install less compiler \e[0m"
sudo apt-get install npm -y
sudo npm install -g less less-plugin-clean-css

echo -e "\e[1;33m - Install wkhtmltopdf 0.12.5 \e[0m"
# ref: http://www.odoo.com/documentation/10.0/setup/install.html
wget https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.5/wkhtmltox_0.12.5-1.bionic_amd64.deb --directory-prefix=/tmp/
sudo apt-get install /tmp/wkhtmltox_0.12.5-1.bionic_amd64.deb -y

echo -e "\e[1;33m - Install Extra tools via Pipx \e[0m"
pipx install git-aggregator --force
pipx install odoo-tools-grap --force
pipx install odoo-module-migrator --force
pipx install pre-commit --force
pipx install virtualenv --force
python3.7 -m pipx ensurepath
source ~/.profile

echo -e "\e[1;33m - Create system and Postgresql User \e[0m"
sudo adduser --system  --shell=/bin/bash  --no-create-home --group odoo12
sudo su postgres -c "psql -f ./install.sql"

echo -e "\e[1;33m - Create Filestore folder \e[0m"
mkdir ./filestore
sudo chgrp odoo12 -R  ./filestore

sudo su odoo12 -c "mkdir ./filestore/github_source_code"

echo -e "\e[1;33m - Install Nginx to manage many port \e[0m"
sudo apt-get install nginx -y

echo -e "\e[1;33m - Create virtualenv \e[0m"
virtualenv env --python=python3.7

echo -e "\e[1;33m - Aggregate repositories of repos.yml \e[0m"
gitaggregate --config repos.yml --log-level WARNING aggregate --jobs=10


echo -e "\e[1;33m - Pin SetupTools version \e[0m"
# Ref : https://github.com/andersinno/suds-jurko/issues/6
# (error in suds-jurko setup command: use_2to3 is invalid.)
./env/bin/python -m pip install "setuptools==58.0.0"

echo -e "\e[1;33m - Install Odoo Python librairies (using legacy-resolver) \e[0m"
# https://stackoverflow.com/questions/69642869/error-in-suds-jurko-setup-command-use-2to3-is-invalid
./env/bin/python -m pip install -r ./src/odoo/requirements.txt --use-deprecated=legacy-resolver

echo -e "\e[1;33m - Install Extra GRAP custom Python librairies \e[0m"
./env/bin/python -m pip install -r ./requirements.txt

echo -e "\e[1;33m - Generate odoo config file \e[0m"
odoo-tools-grap generate
